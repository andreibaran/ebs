﻿using System;

namespace BrokerOverlay
{
    class Startup
    {
		public static void Main()
		{
			BrokerOverlayMaster master = new BrokerOverlayMaster();

			master.Start();

			Console.WriteLine(" Press [enter] to exit.");
			Console.ReadLine();
            master.Stop();
		}
    }
}
