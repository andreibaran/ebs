﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace BrokerOverlay
{
	class BrokerOverlayMaster
	{
		private ConnectionFactory _factory;
		private IConnection _connection;
		private IModel _brokersChannel;
		private IModel _subscriptionsChannel;
		private IModel _publicationsChannel;

		private List<Metadata> _brokersMetadata;
		private int _brokerIndex;

		public BrokerOverlayMaster()
		{
			_factory = new ConnectionFactory() { HostName = "localhost" };
			_connection = _factory.CreateConnection();
			_brokersChannel = _connection.CreateModel();
			_subscriptionsChannel = _connection.CreateModel();
			_publicationsChannel = _connection.CreateModel();

			_brokersMetadata = new List<Metadata>();
			_brokerIndex = -1;
		}

		public void Start()
		{
			RoutePublications();
			Console.WriteLine("*******************************************");
			Console.WriteLine("*            Broker Overlay               *");
			Console.WriteLine("*******************************************");

			HandleBrokers();
			RouteSubscriptions();
		}

		public void Stop()
		{
			_connection?.Dispose();
		}

		/// <summary>
		/// Handle the Brokers.
		/// When new brokers appears in the network, it will automatically try to register on the Broker Overlay.
		/// The Broker Overlay will distribute the work of the registered brokers.
		/// </summary>
		private void HandleBrokers()
		{

			_brokersChannel.ExchangeDeclare(exchange: Constants.BROKER_EXCHANGE_NAME, type: "fanout");

			var brokersQueueName = _brokersChannel.QueueDeclare().QueueName;
			_brokersChannel.QueueBind(queue: brokersQueueName,
							  exchange: Constants.BROKER_EXCHANGE_NAME,
							  routingKey: "");

			Console.WriteLine("Waiting for brokers.");

			var brokersConsumer = new EventingBasicConsumer(_brokersChannel);
			brokersConsumer.Received += (model, ea) =>
			{

				var m = (Message)Utils.ByteArrayToObject(ea.Body);

				if (m.Type == MessageType.Broker)
				{

					var brokerId = ea.BasicProperties.AppId;
					var brokerQueueName = ea.BasicProperties.ReplyTo;

					if (_brokersMetadata.Any(x => x.AppId.Equals(brokerId)))
					{
						Console.WriteLine($"Broker with ID = {brokerId} already exists!");
					}
					else
					{
						Console.WriteLine($"Broker with ID = {brokerId} registered!");
						_brokersMetadata.Add(new Metadata(brokerId, brokerQueueName));
					}

				}
			};

			// register the consumer
			_brokersChannel.BasicConsume(queue: brokersQueueName,
								 noAck: true,
								 consumer: brokersConsumer);

			// TODO unregister a broker ???
		}

		/// <summary>
		/// Routes the subscriptions.
		/// The subscriptions will be disseminate in a Round Robin way to the available brokers.
		/// </summary>
		private void RouteSubscriptions()
		{

			_subscriptionsChannel.ExchangeDeclare(exchange: Constants.SUB_EXCHANGE_NAME, type: "fanout");

			var subscriptionsQueueName = _subscriptionsChannel.QueueDeclare().QueueName;
			_subscriptionsChannel.QueueBind(queue: subscriptionsQueueName,
							  exchange: Constants.SUB_EXCHANGE_NAME,
							  routingKey: "");

			Console.WriteLine("Waiting for subscriptions.");

			var subscriptionConsumer = new EventingBasicConsumer(_subscriptionsChannel);
			subscriptionConsumer.Received += (model, ea) =>
			{

				if (_brokersMetadata.Count > 0)
				{

					// get next broker
					var brokerIndex = GetNextBroker();

					// forward subscription
					var brokerReplyTo = _brokersMetadata[brokerIndex].ReplyToQueueName;
					_brokersChannel.BasicPublish(exchange: "",
									 routingKey: brokerReplyTo,
									 basicProperties: ea.BasicProperties,
									 body: ea.Body);

					// TODO maybe and ACK here for fault tolerance

					var m = (Message)Utils.ByteArrayToObject(ea.Body);

					//Console.WriteLine("Forwarded Subscription: {0} to broker {1}", m.Payload, _brokersMetadata[brokerIndex].AppId);
				}

			};

			// register the consumer
			_subscriptionsChannel.BasicConsume(queue: subscriptionsQueueName,
								 noAck: true,
								 consumer: subscriptionConsumer);
		}

		private void RoutePublications()
		{

			_publicationsChannel.ExchangeDeclare(exchange: "publications", type: "fanout");

			var publicationsQueueName = _publicationsChannel.QueueDeclare().QueueName;
			_publicationsChannel.QueueBind(queue: publicationsQueueName,
							  exchange: "publications",
							  routingKey: "");

			Console.WriteLine(" [*] Waiting for publications.");

			var publicationsConsumer = new EventingBasicConsumer(_publicationsChannel);
			publicationsConsumer.Received += (model, ea) =>
			{

				var m = (Message)Utils.ByteArrayToObject(ea.Body);


				// forward publications to brokers
			};

			// register the consumer
			_publicationsChannel.BasicConsume(queue: publicationsQueueName,
								 noAck: true,
								 consumer: publicationsConsumer);
		}

		/// <summary>
		/// Gets the next broker metadata.
		/// Round Robin
		/// </summary>
		/// <returns>The next broker index.</returns>
		private int GetNextBroker()
		{

			if (_brokerIndex + 1 > _brokersMetadata.Count - 1)
			{
				_brokerIndex = -1; // reset
			}

			_brokerIndex++;
			return _brokerIndex;
		}
	}
}