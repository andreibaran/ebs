﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text.RegularExpressions;
using Broker.Publishers;
using System.Text;

namespace Broker
{
	class Broker
	{
		private ConnectionFactory _factory;
		private IConnection _connection;
		private IModel _brokersChannel;
		private IModel _publicationsChannel;
		private IModel _subscriptionsChannel;
		private List<PublisherModel> _publishers;
		private List<PublisherModel> _matchedPublications;
		private List<PublisherModel> _subscriptions;

		private List<Subscriber> _subscribers;

		private string _brokerReplyQueueName;

		private readonly string _brokerId;

		public Broker()
		{
			_factory = new ConnectionFactory() { HostName = "localhost" };
			_connection = _factory.CreateConnection();

			_brokersChannel = _connection.CreateModel();
			_publicationsChannel = _connection.CreateModel();
			_subscriptionsChannel = _connection.CreateModel();

			_subscribers = new List<Subscriber>();

			_brokerId = Guid.NewGuid().ToString();

			Console.WriteLine("*******************************************");
			Console.WriteLine($"Broker {_brokerId}");
			Console.WriteLine("*******************************************");

			_matchedPublications = new List<PublisherModel>();

		}

		public void Start()
		{
			RegisterBroker();
			HandlePublications();
			HandleSubscriptions();
		}

		public void Stop()
		{
			_connection?.Dispose();
		}

		/// <summary>
		/// Registers the broker.
		/// </summary>
		private void RegisterBroker()
		{

			_brokersChannel.ExchangeDeclare(exchange: Constants.BROKER_EXCHANGE_NAME, type: "fanout");
			_brokerReplyQueueName = _brokersChannel.QueueDeclare().QueueName;

			var props = _brokersChannel.CreateBasicProperties();

			props.AppId = _brokerId;
			props.ReplyTo = _brokerReplyQueueName;

			var m = new Message(MessageType.Broker, "");

			var body = Utils.ObjectToByteArray(m);
			_brokersChannel.BasicPublish(exchange: Constants.BROKER_EXCHANGE_NAME,
								 routingKey: "",
								 basicProperties: props,
								 body: body);

			Console.WriteLine($"Registering Broker with ID {_brokerId}");
		}

		private void HandlePublications()
		{
			_publicationsChannel.ExchangeDeclare(exchange: Constants.PUB_EXCHANGE_NAME, type: "fanout");

			var publicationsQueueName = _publicationsChannel.QueueDeclare().QueueName;
			_publicationsChannel.QueueBind(queue: publicationsQueueName,
							  exchange: Constants.PUB_EXCHANGE_NAME,
							  routingKey: "");

			Console.WriteLine("Waiting for publications.");

			var publicationsConsumer = new EventingBasicConsumer(_publicationsChannel);
			publicationsConsumer.Received += (model, ea) =>
			{

				var m = (Message)Utils.ByteArrayToObject(ea.Body);

				ReadPublishers(m.Payload);

				Console.WriteLine("Publications Read");

				//Console.WriteLine("Publication Received: {0} from PublisherId = {1}", m.Payload, ea.BasicProperties.AppId);

				// TODO do the subscriptions matching
				// for now, just respond to every subscriber
				foreach (var sub in _subscribers)
				{

					// TODO iterate throught subscriber's subscriptions
					foreach (var s in sub.Subscriptions)
					{
						var props = _subscriptionsChannel.CreateBasicProperties();

						props.AppId = sub.Metadata.AppId;
						props.CorrelationId = s.Id;

						// sent the subscription using the reply to queue callback
						_subscriptionsChannel.BasicPublish(exchange: "",
											 routingKey: sub.Metadata.ReplyToQueueName,
											 basicProperties: props,
											 body: ea.Body);
					}
				}


			};

			// register the consumer
			_publicationsChannel.BasicConsume(queue: publicationsQueueName,
								 noAck: true,
								 consumer: publicationsConsumer);
		}

		private void HandleSubscriptions()
		{
			Console.WriteLine("Waiting for subscriptions.");

			var subscriptionConsumer = new EventingBasicConsumer(_subscriptionsChannel);
			subscriptionConsumer.Received += (model, ea) =>
			{

				var m = (Message)Utils.ByteArrayToObject(ea.Body);
				//Console.WriteLine("Subscription Received: {0} from SubscriberId = {1}", m.Payload, ea.BasicProperties.AppId);

				Subscriber sub = _subscribers.Find(x => x.Metadata.AppId.Equals(ea.BasicProperties.AppId));

				if (sub != null)
				{
					Console.WriteLine("Subscriber {0} already exists!", ea.BasicProperties.AppId);
				}
				else
				{
					sub = new Subscriber(new Metadata(ea.BasicProperties.AppId, ea.BasicProperties.ReplyTo));
					_subscribers.Add(sub);
				}

				var subscriptions = m.Payload.Split(new string[] { "};" }, StringSplitOptions.None).ToList();
				var sb = new StringBuilder();
				foreach (var subscription in subscriptions)
				{
					_matchedPublications = _publishers;
					MatchPublications(subscription);

					// string builder is used only to test how many matching publications were made, it should save in a list all the 
					// publications matched and send them back to subscriber
					sb.Append(_matchedPublications.Count);
					sb.Append(" ");
				}

				Console.WriteLine(sb.ToString().Remove(100));
				//TODO send _matchedPublications to the corresponding subscriber

				// TODO check if subscriptions already exists
				sub.Subscriptions.Add(new Subscription(ea.BasicProperties.CorrelationId, m));

			};

			// register the consumer
			_subscriptionsChannel.BasicConsume(queue: _brokerReplyQueueName,
								 noAck: true,
								 consumer: subscriptionConsumer);
		}

		private void MatchPublications(string subscription)
		{
			var subclauses = subscription.Split(';').ToList();
			foreach (var subclause in subclauses)
			{
				if (subclause == string.Empty)
				{
					continue;
				}

				var findBetterName = subclause.Split(',').ToList();
				findBetterName[2] = findBetterName[2].Replace(")", string.Empty).Replace("}", string.Empty);

				if (findBetterName[0].Contains("patient-name"))
				{
					FilterByName(findBetterName[1], findBetterName[2]);
				}
				if (findBetterName[0].Contains("DoB"))
				{
					FilterByDateOfBirth(findBetterName[1], findBetterName[2]);
				}
				if (findBetterName[0].Contains("height"))
				{
					FilterByHeight(findBetterName[1], findBetterName[2]);
				}
				if (findBetterName[0].Contains("eye-color"))
				{
					FilterByEyeColor(findBetterName[1], findBetterName[2]);
				}
				if (findBetterName[0].Contains("heart-rate"))
				{
					FilterByHeartRate(findBetterName[1], findBetterName[2]);
				}
			}
		}

		private void FilterByName(string op, string property)
		{
			if (op == @"=")
			{
				_matchedPublications = _matchedPublications.Where(pub => pub.PatientName == property).ToList();
			}
			else if (op == @"!=")
			{
				_matchedPublications = _matchedPublications.Where(pub => pub.PatientName != property).ToList();
			}
		}

		private void FilterByDateOfBirth(string op, string property)
		{
			var date = Convert.ToDateTime(property);
			if (op == @">")
			{
				_matchedPublications = _matchedPublications.Where(pub => pub.DateOfBirth.Date > date).ToList();
			}
			else if (op == @"<")
			{
				_matchedPublications = _matchedPublications.Where(pub => pub.DateOfBirth.Date < date).ToList();
			}
		}

		private void FilterByHeight(string op, string property)
		{
			if (op == @">")
			{
				_matchedPublications = _matchedPublications.Where(pub => pub.Height > int.Parse(property)).ToList();
			}
			else if (op == @"<")
			{
				_matchedPublications = _matchedPublications.Where(pub => pub.Height < int.Parse(property)).ToList();
			}
		}

		private void FilterByEyeColor(string op, string property)
		{
			if (op == @"=")
			{
				_matchedPublications = _matchedPublications.Where(pub => pub.EyeColor == property).ToList();
			}
			else if (op == @"!=")
			{
				_matchedPublications = _matchedPublications.Where(pub => pub.EyeColor != property).ToList();
			}
		}

		private void FilterByHeartRate(string op, string property)
		{
			if (op == @">")
			{
				_matchedPublications = _matchedPublications.Where(pub => pub.HeartRate > int.Parse(property)).ToList();
			}
			else if (op == @"<")
			{
				_matchedPublications = _matchedPublications.Where(pub => pub.HeartRate < int.Parse(property)).ToList();
			}
		}

		private void ReadPublishers(string fileContent)
		{
			List<PublisherModel> publishers = new List<PublisherModel>();

			var regex = new Regex("{(.*?)}");
			var regexName = new Regex("patient-name,(.*?);");
			var regexDob = new Regex("DoB,(.*?);");
			var regexHeight = new Regex("height,(.*?);");
			var regexEye = new Regex("eye-color,(.*?);");
			var regexHeart = new Regex("heart-rate,(.*?)}");

			var records = regex.Matches(fileContent);

			foreach (var record in records)
			{
				var recordString = record.ToString();
				var name = regexName.Match(recordString).Groups[1].ToString();
				var dateOfBirth = regexDob.Match(recordString).Groups[1].ToString();
				var height = regexHeight.Match(recordString).Groups[1].ToString();
				var eyeColor = regexEye.Match(recordString).Groups[1].ToString();
				var heartRate = regexHeart.Match(recordString).Groups[1].ToString();
                try{
					var result = new PublisherModel
					{
						PatientName = name.Remove(name.Length - 1),
						DateOfBirth = Convert.ToDateTime(dateOfBirth.Remove(dateOfBirth.Length - 1)),
						Height = double.Parse(height.Remove(height.Length - 1)),
						EyeColor = eyeColor.Remove(eyeColor.Length - 1),
						HeartRate = int.Parse(heartRate.Remove(heartRate.Length - 1))
					};

					publishers.Add(result);
                } catch(Exception e) {
                    Console.WriteLine(e.Message);
                }

			}

			_publishers = publishers;
		}
	}
}