﻿using System;

namespace Broker.Publishers
{
    public class PublisherModel
    {
        public string PatientName { get; set; }

        public DateTime DateOfBirth { get; set; }

        public double Height { get; set; }

        public string EyeColor { get; set; }

        public int HeartRate { get; set; }
    }
}
