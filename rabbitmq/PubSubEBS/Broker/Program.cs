﻿using System;

namespace Broker
{
    class Program
    {
        public static void Main(string[] args)
        {
            var broker = new Broker();

            broker.Start();

			Console.WriteLine(" Press [enter] to exit.");
			Console.ReadLine();
            broker.Stop();
        }
    }
}
