﻿using System;

namespace Publisher
{
	class Startup
	{
		public static void Main(string[] args)
		{
			var publisher = new Publisher();

			publisher.Start();

			Console.WriteLine(" Press [enter] to exit.");
			Console.ReadLine();
			publisher.Stop();
		}
	}
}