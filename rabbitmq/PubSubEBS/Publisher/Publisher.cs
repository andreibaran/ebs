﻿using System;
using Common;
using RabbitMQ.Client;
using System.IO;

namespace Publisher
{
	public class Publisher
	{
		private ConnectionFactory _factory;
		private IConnection _connection;
		private IModel _channel;

		private string _publisherId;


		public Publisher()
		{
			_factory = new ConnectionFactory() { HostName = "localhost" };
			_connection = _factory.CreateConnection();
			_channel = _connection.CreateModel();

			_publisherId = Guid.NewGuid().ToString();
			Console.WriteLine("*******************************************");
			Console.WriteLine($"Publisher {_publisherId}");
			Console.WriteLine("*******************************************");
		}

		public void Start()
		{
			_channel.ExchangeDeclare(exchange: Constants.PUB_EXCHANGE_NAME, type: "fanout");

			Console.WriteLine("Press [stop] to exit.");
			Console.WriteLine("Publisher >> Press [start] to generate publications.");

			// TODO send publication during a pecific period
			// TODO read publications from homework 1 or from public data sets -> DONE
			var message = String.Empty;
			while (!message.Equals("stop"))
			{
				var props = _channel.CreateBasicProperties();
				props.AppId = _publisherId;

				message = Console.ReadLine();
				var generate = false;
				if (message.ToLower() == "start")
				{
					message = ReadPublications();
					generate = true;
				}

				var m = new Message(MessageType.Publication, message);

				var body = Utils.ObjectToByteArray(m);

				_channel.BasicPublish(exchange: Constants.PUB_EXCHANGE_NAME,
									 routingKey: "",
									 basicProperties: props,
									 body: body);

				if (generate)
				{
					Console.WriteLine(" [x] Generation done");
				}
				else
				{
					Console.WriteLine(" [x] Sent {0}", message);
				}
			}
		}

		public void Stop()
		{
			_connection?.Dispose();
		}

		private static string ReadPublications()
		{
			var publications = string.Empty;
			var path = Path.Combine(Environment.CurrentDirectory, "Publishers" + Path.DirectorySeparatorChar + "Publishers.txt");
			path = path.Replace(Path.DirectorySeparatorChar + "bin" + Path.DirectorySeparatorChar + "Debug", string.Empty);
			using (StreamReader r = new StreamReader(path))
			{
				publications = r.ReadToEnd();
			}
			return publications;
		}
	}
}