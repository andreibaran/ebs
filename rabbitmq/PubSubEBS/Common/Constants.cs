﻿namespace Common
{
    public static class Constants
    {
		public const string SUB_EXCHANGE_NAME = "subscriptions";
        public const string PUB_EXCHANGE_NAME = "publications";
		public const string BROKER_EXCHANGE_NAME = "brokers";

    }
}
