﻿using System;
namespace Common
{
    public enum MessageType {
        Subscription, 
        Publication,
        Broker
    }

    [Serializable]
    public class Message
    {
        public Message(MessageType mType, string mPayload)
        {
            Type = mType;
            Payload = mPayload;
        }

        public MessageType Type { get; set; }

        public string Payload { get; set; }
    }
}
