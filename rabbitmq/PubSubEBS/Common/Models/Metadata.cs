﻿using System;
namespace Common
{
    public class Metadata
    {
        public Metadata(string appId, string replyTo)
        {
            AppId = appId;
            ReplyToQueueName = replyTo;
        }

        public string AppId { get; set; }
        public string ReplyToQueueName { get; set; }
    }
}
