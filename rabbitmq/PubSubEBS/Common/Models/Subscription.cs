﻿using System;
namespace Common.Models
{
    public class Subscription
    {
        public Subscription()
        {
        }

        public Subscription(string id, Message m)
		{
            Id = id;
            Message = m;
		}

        public string Id { get; set; }

        // TODO this is temporary. We need to create a proper model: operator, values, type
        // TODO model the data and the operator comparators that will be used at the mathing step
        public Message Message { get; set; }
    }
}
