﻿using System;
using System.Collections.Generic;

namespace Common.Models
{
    public class Subscriber
    {
        public Subscriber()
        {
        }

        public Subscriber(Metadata meta) {
            Metadata = meta;
            Subscriptions = new List<Subscription>();
        }

        public Metadata Metadata { get; set; }
        public List<Subscription> Subscriptions { get; set; }
    }
}
