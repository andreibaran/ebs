﻿using System;

namespace Subscriber
{
	class Program
	{
		public static void Main(string[] args)
		{
			var subscriber = new Subscriber();

			subscriber.Start();

			Console.WriteLine(" Press [enter] to exit.");
			Console.ReadLine();
			subscriber.Stop();
		}
	}
}