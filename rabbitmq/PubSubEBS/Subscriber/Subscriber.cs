﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.IO;

namespace Subscriber
{
	public class Subscriber
	{

		private ConnectionFactory _factory;
		private IConnection _connection;
		private IModel _channel;

		private List<string> subscriptionIDs; // identifies all subscriptions requests by using GUIDs

		private string _replyQueueName; // queue used for reply
		private string _subscriberId;

		public Subscriber()
		{
			_factory = new ConnectionFactory() { HostName = "localhost" };
			_connection = _factory.CreateConnection();
			_channel = _connection.CreateModel();

			subscriptionIDs = new List<string>();

			_channel.ExchangeDeclare(exchange: Constants.SUB_EXCHANGE_NAME, type: "fanout");

			_replyQueueName = _channel.QueueDeclare().QueueName;
			_subscriberId = Guid.NewGuid().ToString();

			Console.WriteLine("*******************************************");
			Console.WriteLine($"Subscriber {_subscriberId}");
			Console.WriteLine("*******************************************");
		}

		public void Start()
		{

			var consumer = new EventingBasicConsumer(_channel);

			// publications received that match with the subscription made
			consumer.Received += (model, ea) =>
			{
				if (subscriptionIDs.Any(x => x.Equals(ea.BasicProperties.CorrelationId)))
				{

					var m = (Message)Utils.ByteArrayToObject(ea.Body);

					Console.WriteLine("Publication {0} from BrokerId = {1}", m.Payload, ea.BasicProperties.AppId);
				}
			};

			// bind the consumer
			_channel.BasicConsume(queue: _replyQueueName,
								 noAck: true,
								 consumer: consumer);


			// start sending subscriptions
			Console.WriteLine("Press [stop] to exit.");


			// TODO construct subscriptions based on specification
			// TODO 30000 subscriptions: based on a interval register subscriptions
			// TODO read subscriptions from public data sets or from homework 1
			var message = String.Empty;
			while (!message.Equals("stop"))
			{

				// create a correlation ID for subscription
				var corrId = Guid.NewGuid().ToString();
				var props = _channel.CreateBasicProperties();

				props.AppId = _subscriberId;        // subscriber Id
				props.ReplyTo = _replyQueueName;    // subscriber callback queue
				props.CorrelationId = corrId;       // subscription Id

				subscriptionIDs.Add(corrId);

				message = Console.ReadLine();
				var generate = false;
				if (message.ToLower() == "start")
				{
					message = ReadSubscriptions();
					generate = true;
				}

				var m = new Message(MessageType.Subscription, message);

				var body = Utils.ObjectToByteArray(m);

				// sent the subscription
				// here I send all the subscriptions from a subscriber, should be sent one subscription at a time?
				_channel.BasicPublish(exchange: Constants.SUB_EXCHANGE_NAME,
									 routingKey: "",
									 basicProperties: props,
									 body: body);

				if (generate)
				{
					Console.WriteLine(" [x] Subsricptions sent");
				}
				else
				{
					Console.WriteLine(" [x] Sent {0}", message);
				}
			}

		}

		public void Stop()
		{
			_connection?.Dispose();
		}

		private static string ReadSubscriptions()
		{
			// after first 30000 subscription are send, the file should be renamed to be sure that next subscriber will use a different set of data
			var subscriptions = string.Empty;
			var path = Path.Combine(Environment.CurrentDirectory, "Subscribers" + Path.DirectorySeparatorChar + "Subscribers1.txt");
			path = path.Replace(Path.DirectorySeparatorChar + "bin" + Path.DirectorySeparatorChar + "Debug", string.Empty);

			if (!File.Exists(path))
			{
				path = Path.Combine(Environment.CurrentDirectory, "Subscribers"+ Path.DirectorySeparatorChar + "Subscribers2.txt");
				path = path.Replace(Path.DirectorySeparatorChar + "bin" + Path.DirectorySeparatorChar + "Debug", string.Empty);
				if (!File.Exists(path))
				{
					path = Path.Combine(Environment.CurrentDirectory, "Subscribers" + Path.DirectorySeparatorChar + "Subscribers3.txt");
					path = path.Replace(Path.DirectorySeparatorChar + "bin" + Path.DirectorySeparatorChar + "Debug", string.Empty);
				}
			}

			using (StreamReader r = new StreamReader(path))
			{
				subscriptions = r.ReadToEnd();
			}
			return subscriptions;
		}
	}
}